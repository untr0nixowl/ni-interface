﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Diagnostics;
using System.Windows.Threading;

namespace AutomaticDeviceCalibration.ViewModels
{
   public class DeviceControlViewModel:Base.BaseViewModel
    {
        private Visibility isLoading;
        private Double opacity;
        private ObservableCollection<DataModels.Device> mDevices;
        private DataModels.Device selectedDevice;
        private String totalCpu;
        private String totalRam;
        private PerformanceCounter cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
        private PerformanceCounter ramCounter = new PerformanceCounter("Memory", "Available MBytes");
        public Visibility IsLoading
        {
            get => isLoading;
            set
            {
                isLoading = value;
                OnPropertyChanged("IsLoading");
            }
        }
        public ObservableCollection<DataModels.Device> Devices
        {
            get => mDevices;
            set
            {
                if (value == null)
                    return;
                mDevices = value;
                OnPropertyChanged("Devices");
            }
        }
        public Double MyOpacity
        {
            get => opacity;
            set
            {
                opacity = value;
                OnPropertyChanged("MyOpacity");
            }
        }
        public String TotalRam
        {
            get => totalRam;
            set
            {
                if (value == null)
                    return;
                totalRam = value;
                OnPropertyChanged("TotalRam");
            }
        }
        public DataModels.Device SelectedDevice
        {
            get => selectedDevice;
            set
            {
                if (value == null)
                    return;
                selectedDevice = value;
                OnPropertyChanged("SelectedDevice");
            }
        }
        public String TotalCpu
        {
            get => totalCpu;
            set
            {
                totalCpu = value;
                OnPropertyChanged("TotalCpu");
            }
        }
        public Visibility Visible { get; set; }
        public ICommand RebootCommand { get; set; }
        public ICommand RebootNetworkCommand { get; set; }
        public ICommand RebootEth0Command { get; set; }
        public ICommand GetCpuStatsCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand SaveCommand { get; set; }

        public ICommand RemoveCommand { get; set; }

       public DeviceControlViewModel(ObservableCollection<DataModels.Device> devices)
        {
            Devices = devices;
            TotalCpu = cpuCounter.NextValue() + "%";
            TotalRam = ramCounter.NextValue() + "MB";
            SelectedDevice = Devices.First();
            CommandInit();
            MyOpacity = 1;
            IsLoading = Visibility.Hidden;
            Visible = Visibility.Visible;
        }
      

        private void CommandInit()
        {
            RebootCommand = new Base.RelayCommand(Reboot);
            RebootNetworkCommand = new Base.RelayCommand(NetworkReboot);
            RebootEth0Command = new Base.RelayCommand(NetworkRebootEth0Protocol);
            GetCpuStatsCommand = new Base.RelayCommand(CpuStatsThreads);
            AddCommand = new Base.RelayCommand(Add);
            RemoveCommand = new Base.RelayCommand(Remove);
            SaveCommand = new Base.RelayCommand(Save);
            EventInit();
        }
        private void EventInit()
        {
            Helpers.TimeHelper.DispatcherTimerv2.Tick += new EventHandler(MyCpuThreads);
            Helpers.TimeHelper.DispatcherTimerv2.Interval = new TimeSpan(0,0,5);
            Helpers.TimeHelper.DispatcherTimerv2.Start();
        }

        private void NetworkRebootEth0Protocol()
        {
            SshCommands.SshExecuter.RestartNetworkingProtocol(SelectedDevice.AddressIP, SelectedDevice.UserName, SelectedDevice.Password, SelectedDevice.Port);
        }
        private void ArrInit(List<String> lst)
        {
            SelectedDevice.UserCpu = lst[0];
            SelectedDevice.SystemCpu = lst[1];
            SelectedDevice.IdleCpu = lst[2];
        }
        private void CpuStats()
        {
            MyOpacity = 0;
            TotalCpu = SshCommands.SshExecuter.GetAverallCpuUsage(SelectedDevice.AddressIP, SelectedDevice.UserName, SelectedDevice.Password, SelectedDevice.Port);
            var lst = SshCommands.SshExecuter.GetDetailedCpuUsage(SelectedDevice.AddressIP, SelectedDevice.UserName, SelectedDevice.Password, SelectedDevice.Port);
            SelectedDevice.UserCpu = lst[0];
            SelectedDevice.SystemCpu = lst[1];
            SelectedDevice.IdleCpu = lst[2];
            OnPropertyChanged("SelectedDevice");
        }

        private void CpuStatsThreads()
        {
            IsLoading = Visibility.Visible;
            Task.Factory.StartNew(() => CpuStats()).ContinueWith((task) =>
            {
                IsLoading = Visibility.Hidden;
                MyOpacity = 1;
               
            }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());

        }
        private void MyCpuThreads(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() => LoadMyCpu() ).ContinueWith((task) => { }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void NetworkReboot()
        {
            SshCommands.SshExecuter.RestartNetworking(SelectedDevice.AddressIP, SelectedDevice.UserName, SelectedDevice.Password, SelectedDevice.Port);
        }

        private void Reboot()
        {
            SshCommands.SshExecuter.RebootMachine(SelectedDevice.AddressIP, SelectedDevice.UserName, SelectedDevice.Password, SelectedDevice.Port);
        }
        private void Add()
        {
            var newDevice = new DataModels.Device() {AddressIP = "1.1.1.1" };
            Devices.Add(newDevice);
            var index = Devices.IndexOf(newDevice);
            SelectedDevice = Devices[index];
        }
        private void Remove()
        {
            Devices.Remove(SelectedDevice);
            SelectedDevice = Devices[0];
        }
        private void Save()
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"device.csv"))
            {
                foreach(var device in Devices)
                {
                    file.WriteLine(device.AddressIP + "," + device.UserName + "," + device.Password + "," + device.Port + "," + device.FileName + "," + device.PythonFilePath);
                }
            }
        }
        private void LoadMyCpu()
        {
            TotalCpu = cpuCounter.NextValue() + "%";
            TotalRam = ramCounter.NextValue() + "MB";

        }
    }
}
