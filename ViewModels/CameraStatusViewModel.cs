﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace AutomaticDeviceCalibration.ViewModels
{
    public class CameraStatusViewModel: Base.BaseViewModel
    {
        private float opacity;
        private Visibility isLoading;
        private ObservableCollection<DataModels.Device> mDevices;
        private DataModels.Device selectedDevice;
        private DataModels.CameraState selectedCamera;
      

        public float MyOpacity
        {
            get => opacity;
            set
            {
                opacity = value;
                OnPropertyChanged("MyOpacity");
            }
        }
        public Visibility IsLoading
        {
            get => isLoading;
            set
            {
                isLoading = value;
                OnPropertyChanged("IsLoading");
            }
        }
        public ObservableCollection<DataModels.Device> Devices
        {
            get => mDevices;
            set
            {
                if (value == null || mDevices == value)
                    return;
                mDevices = value;
                OnPropertyChanged("Devices");
            }
        }
        
        public DataModels.Device SelectedDevice
        {
            get => selectedDevice;
            set
            {
                if (value == null)
                    return;
                selectedDevice = value;
                OnPropertyChanged("SelectedDevice");
            }
        }

        public ObservableCollection<DataModels.CameraState> CameraInfos
        {
            get => SelectedDevice.CameraStates;
            set
            {
                if (value == null || value == SelectedDevice.CameraStates)
                    return;
                SelectedDevice.CameraStates = value;
                OnPropertyChanged("CameraInfos");
            }
        }
        public DataModels.CameraState SelectedCamera
        {
            get => selectedCamera;
            set
            {
                if (value == null || selectedCamera == value)
                    return;
                selectedCamera = value;
                OnPropertyChanged("SelectedCamera");
            }
        }
      
        public ICommand ForcePingCommand { get; set; }


        public CameraStatusViewModel()
        {
        }

        public CameraStatusViewModel(ObservableCollection<DataModels.Device> devices)
        {
            Devices = devices;
            Task.Run(() => { Parallel.ForEach(devices, (device) => { IsLoading = Visibility.Visible; device.CameraStates = Parsers.CsvParser.ParseState(SshCommands.SshExecuter.GetCurrentStatus(device.AddressIP, device.UserName, device.Password, device.FileName, device.Port)); }); }).ContinueWith((t) =>
            {
                if (!t.IsFaulted)
                {
                    Devices = devices;
                    SelectedDevice = Devices[0];
                    SelectedCamera = SelectedDevice.CameraStates.FirstOrDefault();
                    IsLoading = Visibility.Hidden;
                }

            }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());

            IsLoading = Visibility.Hidden;
            MyOpacity = 1;
            EventInit();
        }
        private void EventInit()
        {
            Helpers.TimeHelper.DispatcherTimer.Tick += new EventHandler(LoadPingDataThread);
            Helpers.TimeHelper.DispatcherTimer.Interval = new TimeSpan(0, 0, 20);
            Helpers.TimeHelper.DispatcherTimer.Start();
        }
        private void ForcePing()
        {
            SshCommands.SshExecuter.ForcePing(SelectedDevice.AddressIP, SelectedDevice.UserName, SelectedDevice.Password, SelectedDevice.Port);

        }
        private void LoadPingData()
        {
            try
            {
                Parallel.ForEach(Devices, (device) => { device.CameraStates = Parsers.CsvParser.ParseState(SshCommands.SshExecuter.GetCurrentStatus(device.AddressIP, device.UserName, device.Password, device.FileName, device.Port)); });
                foreach (var device in Devices)
                    if (SelectedDevice.AddressIP == device.AddressIP)
                    {
                        SelectedDevice = device;
                        break;
                    }
            } catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }
        private void LoadPingDataThread(object sender, EventArgs e)
        {
            IsLoading = Visibility.Visible;
            MyOpacity = 0;
            Task.Factory.StartNew(() => LoadPingData()).ContinueWith((task) =>
            {
                IsLoading = Visibility.Hidden;
                MyOpacity = 1;
            }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());
        }

    }
}
