﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AutomaticDeviceCalibration.SshCommands
{
    internal class SshExecuter
    {
        #region Private Members
        private static String ExecuteSshCommand(ConnectionInfo connectionInfo, String commandString)
        {
            String text;
            using (SshClient client = new SshClient(connectionInfo))
            {
                try
                {
                    client.Connect();
                    using (SshCommand command = client.CreateCommand(commandString))
                    {
                        text = command.Execute();
                    }
                    client.Disconnect();
                    return text;
                }
                catch (Exception e)
                {
                    MessageBox.Show(connectionInfo.Host + ": " + e.Message); 
                    throw e;
                }
            }
        }
        private static ConnectionInfo CreatePasswordCredentials(String host, String username, String password, Int32 port = 22)
        {
            var methods = new List<AuthenticationMethod>() { new PasswordAuthenticationMethod(username, password) };
            ConnectionInfo cred = new ConnectionInfo(host, port, username, methods.ToArray());
            return cred;
        }
        #endregion
        #region Public Members
        public static String GetCurrentStatus(String host, String username, String password, String filename, Int32 port = 22)
        {
            return GetCurrentStatus(CreatePasswordCredentials(host, username, password, port), filename);
        }
        public static String RebootMachine(String host, String username, String password, Int32 port)
        {
            return RebootMachine(CreatePasswordCredentials(host, username, password, port));
        }
        public static String RestartNetworking(String host, String username, String password, Int32 port = 22)
        {
            return RestartNetworking(CreatePasswordCredentials(host, username, password, port));
        }

        public static String ForcePing(String host, String username, String password, Int32 port = 22)
        {
            return ForcePing(CreatePasswordCredentials(host, username, password, port));
        }

        public static String GetAverallCpuUsage(String host, String username, String password, Int32 port = 22)
        {
            return GetAverallCpuUsage(CreatePasswordCredentials(host, username, password, port));
        }
        public static List<String> GetDetailedCpuUsage(String host, String username, String password, Int32 port = 22)
        {
            return GetDetailedCpuUsage(CreatePasswordCredentials(host, username, password, port));
        }

        public static String RestartNetworkingProtocol(String host, String username, String password, Int32 port = 22, String protocol = "eth0")
        {
            return RestartNetworkingProtocol(CreatePasswordCredentials(host, username, password, port), protocol);
        }
        public static String GetCurrentStatus(ConnectionInfo connectionInfo, String filename)
        {
            return ExecuteSshCommand(connectionInfo, $"cat {filename}");
        }

        public static String RestartNetworking(ConnectionInfo connectionInfo)
        {
            return ExecuteSshCommand(connectionInfo, @"sudo /etc/init.d/networking restart");
        }
        public static String RestartNetworkingProtocol(ConnectionInfo connectionInfo, String protocol = "eth0")
        {
            return ExecuteSshCommand(connectionInfo, $"sudo ifconfig {protocol} down & sudo ifconfig {protocol} up");
        }

        public static String ForcePing(ConnectionInfo connectionInfo)
        {
            return ExecuteSshCommand(connectionInfo, $"python scan.py -m interactive");
        }

        public static String GetAverallCpuUsage(ConnectionInfo connectionInfo)
        {

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("top -bn1|");
            stringBuilder.Append("grep \"Cpu(s)\"|");
            stringBuilder.Append("sed \"s/.*, *\\([0-9.]*\\)%* id.*/\\1/\"|");
            stringBuilder.Append("awk '{print 100 - $1\" % \"}'");
            return ExecuteSshCommand(connectionInfo, stringBuilder.ToString());
        }
        /// <summary>
        /// Experimental function, works with mpstat on remote machine
        /// </summary>
        /// <param name="connectionInfo"></param>
        /// <returns></returns>
        public static List<String> GetDetailedCpuUsage(ConnectionInfo connectionInfo)
        {
            var result  = ExecuteSshCommand(connectionInfo, "mpstat");
            return Parsers.SimpleAnswerParser.ParseSimpleAnswer(result);

        }
        public static String RebootMachine(ConnectionInfo connectionInfo)
        {
            return ExecuteSshCommand(connectionInfo, "sudo reboot");
        }
        #endregion
    }
}
