﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FontAwesome.WPF;
namespace AutomaticDeviceCalibration
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ObservableCollection<DataModels.Device> Machines;
        private ViewModels.DeviceControlViewModel pageOne;
        private ViewModels.CameraStatusViewModel pageTwo;
        public MainWindow()
        {
            
            try
            {
                InitializeComponent();
                Machines = Parsers.CsvParser.ReadConfigFromCsv("device.csv");
                pageOne = new ViewModels.DeviceControlViewModel(Machines);
                /*
                Task.Run(() => { Parallel.ForEach(Machines, (device) => { device.CameraStates = Parsers.CsvParser.ParseState(SshCommands.SshExecuter.GetCurrentStatus(device.AddressIP, device.UserName, device.Password, device.FileName, device.Port)); }); }).ContinueWith((t) => 
                {
                    if(!t.IsFaulted)
                        pageTwo = new ViewModels.CameraStatusViewModel(Machines);

                }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());
                */
                pageTwo = new ViewModels.CameraStatusViewModel(Machines);
                
                DataContext = pageOne;
                GridCursor.Margin = new Thickness(10 + (390 * 1), 40, 0, 0);


            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                //Application.Current.Shutdown();
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int index = int.Parse(((Button)e.Source).Uid);
            GridCursor.Margin = new Thickness(10 + (390 * index), 40, 0,0);
            switch (index)
            {
                case 0:
                    {
                        try
                        {
                            pageTwo.Devices = Machines;
                            DataContext = pageTwo;
                        }
                        catch (Exception)
                        {
                            DataContext = pageOne;
                        }
                    }
                    break;
                case 1:
                    {
                        Machines = Parsers.CsvParser.ReadConfigFromCsv("device.csv");
                        pageOne.Devices = Machines;
                        DataContext = pageOne;
                    }
                    break;
            }
        }
    }
}
