﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomaticDeviceCalibration.DataModels
{
    public class Device
    {
        public String AddressIP { get; set; }
        public String UserName { get; set; }
        public String Password { get; set; }
        public Int32 Port { get; set; } = 22;
        public String FileName { get; set; }
        public String PythonFilePath { get; set; }
        public String TotalCpu { get; set; }
        public String UserCpu { get; set; }
        public String SystemCpu { get; set; }
        public String IdleCpu { get; set; }
        public ObservableCollection<CameraState> CameraStates { get; set; }
        public override string ToString()
        {
            return AddressIP;
        }
    }
}
