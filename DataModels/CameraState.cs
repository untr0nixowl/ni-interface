﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomaticDeviceCalibration.DataModels
{
    public class CameraState
    {
        public String CameraIP { get; set; }
        public Boolean IsEnabled { get; set; }
        public override string ToString()
        {
            return CameraIP;
        }
    }
}
