﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AutomaticDeviceCalibration.Parsers
{
    class CsvParser
    {
        public static ObservableCollection<DataModels.CameraState> ParseState(String state)
        {

                ObservableCollection<DataModels.CameraState> stateRes = new ObservableCollection<DataModels.CameraState>();
                var stateList = state.Trim().Split('\n');
                foreach (var stateItem in stateList)
                {
                    Boolean isEnabled = (stateItem.Split(',')[1] == "true") ? true : false;
                    stateRes.Add(new DataModels.CameraState { CameraIP = stateItem.Split(',')[0], IsEnabled = isEnabled });

                }
                return stateRes;
        }
        public static ObservableCollection<DataModels.Device> ParseConfigCsv(String config)
        {
            ObservableCollection<DataModels.Device> configRes = new ObservableCollection<DataModels.Device>();
            var configList = config.Trim().Split('\n');
            foreach (var configItem in configList)
            {
                var parameters = configItem.Split(',');
                if (parameters.Length < 6)
                    throw new System.ArgumentException("you must contain info about IpAddress Login Password Port,Ping info FilePath and Python ForcePing script path");
                configRes.Add(new DataModels.Device { AddressIP = parameters[0].Trim(), UserName = parameters[1].Trim(), Password = parameters[2].Trim(), Port = Int32.Parse(parameters[3].Trim()), FileName = parameters[4].Trim(), PythonFilePath = parameters[5].Trim() });
            }
            return configRes;

        }
        public static ObservableCollection<DataModels.Device> ReadConfigFromCsv(string configPath)
        {
            string configText = System.IO.File.ReadAllText(configPath);
            return ParseConfigCsv(configText);
        }
    }
}
