﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AutomaticDeviceCalibration.Parsers
{
    public class SimpleAnswerParser
    {
        public static List<String> ParseSimpleAnswer(String response)
        {
            List<String> lst = new List<string>();
            var splits = response.Split('\n')[3];
            var stats = splits.Split(' ').ToList();
            stats.ForEach(str => str.Trim());
            stats.RemoveAll(str => String.IsNullOrEmpty(str));
            lst.Add(stats[2]);
            lst.Add(stats[5]);
            lst.Add(stats[stats.Count - 1]);
            MessageBox.Show(lst[0]);
            return lst;


        }
    }
}
