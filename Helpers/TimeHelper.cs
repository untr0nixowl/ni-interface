﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace AutomaticDeviceCalibration.Helpers
{
    internal class TimeHelper
    {
        public static DispatcherTimer DispatcherTimer { get; set; }
        public static DispatcherTimer DispatcherTimerv2 { get; set; }
        static TimeHelper()
        {
            DispatcherTimer = new DispatcherTimer();
            DispatcherTimerv2 = new DispatcherTimer();
        }
    }
}
